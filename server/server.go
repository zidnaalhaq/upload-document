package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func uploadFile(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Upload File\n")

// 1. parse input, type multipart/form-data
	r.ParseMultipartForm(10 << 20)

// 2. retrieve file from posted form-data
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("error retrieving file from form-data")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded file : %+v\n", handler.Filename)
	fmt.Printf("file Seze : %+v\n", handler.Size)
	fmt.Printf("MIME header : %+v\n", handler.Header)

// 3. write temporary file on our server
	tempFile, err := ioutil.TempFile("temp-images", "upload-*.pdf")
	if err != nil {
		fmt.Println(err)
		return
	}

	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	tempFile.Write(fileBytes)

// 4. return whether or not this hahs been successfull
	fmt.Fprintf(w, "Succesfully Upload")
}

func SetupRoutes() {
	http.HandleFunc("/Upload", uploadFile)
	http.ListenAndServe(":8080", nil)
}

func main(){
	fmt.Println("Go file upload")
	SetupRoutes()
}